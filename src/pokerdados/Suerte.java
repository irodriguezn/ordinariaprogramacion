/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokerdados;

/**
 *
 * @author nacho
 */
public class Suerte {

    public static int aleatorio(int inicio, int fin) {
        int intervalo = fin - inicio + 1;
        return (int) (Math.random() * intervalo + inicio);
    }

    public static int aleatorio(int fin) {
        return (int) (Math.random() * fin + 1);
    }
}
