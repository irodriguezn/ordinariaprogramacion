package pokerdados;

import java.util.ArrayList;

/**
 *
 * @author Nacho Rodríguez
 */
public class GrpDados {

    ArrayList<Dado> dados = new ArrayList<>();

    public GrpDados() {
    }

    public GrpDados(int num) {
        Dado d1;
        for (int i = 0; i < num; i++) {
            d1 = new Dado();
            d1.lanza();
            dados.add(d1);
        }
    }
    
    public GrpDados(int[] valores) {
        Dado d;
        for (int i=0; i<valores.length; i++) {
            d=new Dado();
            d.setValor(valores[i]);
            dados.add(d);
        }
    }

    public void tira() {
        for (Dado d : dados) {
            d.lanza();
        }
    }

    public void ordena() {
        dados.sort(null);
    }

    public ArrayList<Dado> getDados() {
        return dados;
    }
    
    public void añadeDados(GrpDados grupo) {
        for (Dado d:grupo.getDados()) {
            dados.add(d);
        }
    }
    
    public void addDado(Dado d) {
        dados.add(d);
    }
    
    public Dado getDado(int pos) {
        Dado d=dados.get(pos);
        dados.remove(pos);
        return d;
    }

    @Override
    public String toString() {
        return dados + "";
    }
    
    public int getPuntuacion(){
        int puntos=0;
        for (Dado d:dados) {
            puntos+=d.getValor();
        }
        return puntos;
    }
    
    public int getJugada(){
        return devuelveValorJugada(devuelveDistintos());
    }
    
    private ArrayList devuelveDistintos(){
        ArrayList distintos=new ArrayList();
        for (Dado d:dados) {
            if (!contiene(distintos,d)) { 
                distintos.add(d);
            }
        }
        return distintos;
    }
    
    private int devuelveValorJugada(ArrayList distintos) {
        int valor=0;
        switch (distintos.size()) {
            case 1:
                valor=7; //Repoker
                break;
            case 2: //Puede ser full o poker 
                Dado candidato=(Dado)distintos.get(0);
                int contador=0;
                for (Dado d:dados) {
                    if (d.getValor()==candidato.getValor()) {
                        contador++;
                    }
                }
                if (contador==1 || contador==4) {
                    valor=6; //Poker
                } else {
                    valor=5; //Full
                }
                break;
            case 5: //O escalera o nada
                int puntos=getPuntuacion();
                if (puntos==20 || puntos==15) {
                    valor=4; //escalera
                } 
                break; 
            case 4:
                valor=1; //pareja
                break;
            case 3: //Trío o dobles
                candidato=(Dado)distintos.get(0);
                contador=0;
                for (Dado d:dados) {
                    if (d.getValor()==candidato.getValor()) {
                        contador++;
                    }
                }
                if (contador==3) {
                    valor=3; //Trío
                } else if (contador==2){
                    valor=2; //Dobles
                } else {
                    candidato=(Dado)distintos.get(1);
                    contador=0;
                    for (Dado d:dados) {
                        if (d.getValor()==candidato.getValor()) {
                            contador++;
                        }
                    }
                    if (contador==2) {
                        valor=2; //Dobles
                    } else {
                        valor=3; //Trio
                    }
                }
        }
        return valor;
    }

    /* Jugadas poker
        7- repoker
        6- poker
        5- full
        4- escalera RJQKA (20) o NRJQK (15) (no vale ANRJQ)
        3- trio
        2- dobles parejas
        1- pareja
        0- nada ANRJQ valor=16 AKNRJ 17 AKQRN 18 AKQJN 19
    */
    
    private boolean contiene (ArrayList distintos, Dado d) {
        Dado d1;
        boolean esta=false;
        for (int i=0; i<distintos.size();i++) {
            d1=(Dado)distintos.get(i);
            if (d1.getValor()==d.getValor()) {
                esta=true;
                break;
            }
        }
        return esta;
    }
}
